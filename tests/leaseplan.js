import {short, medium, long} from '../lib/timeouts'
import {mobile,tablet,desktop} from '../lib/devices'
import App from '../page-objects/App'
import ShowroomPage from '../page-objects/pages/ShowroomPage'


describe("Load Leaseplan page", () => {
    it("Leaseplan Demo TestCase", () => {
        
        //this testcase is using a simple pageobjects model in javascript, leveraging on the ShowroomPage and inheriting pause from the base class
        App.openLeasePlanShowroom()
        browser.setWindowSize(tablet[0], tablet[1])
        ShowroomPage.pauseShort()
        ShowroomPage.CookieIsVisible()
        ShowroomPage.ClickonAcceptCookie()
        ShowroomPage.pauseLong()

      //  browser.url("https://www.leaseplan.com/en-be/business/showroom/");
      //  browser.pause(short)
      //  const buttonClickCookie = $("[class$='optanon-allow-all accept-cookies-button']")
      //  buttonClickCookie.waitForExist()
      //  buttonClickCookie.click()
      //  browser.pause(long)
     
        expect(browser).toHaveUrl("https://www.leaseplan.com/en-be/business/showroom/")
    })
    it("Select BMW Model", () => {
          
        const clickonMakeaModel = $("//div[contains(@data-component,'desktop-filters')]/descendant::h3[contains(@data-key,'Make')]/ancestor::button/div")
        browser.pause(short)
        clickonMakeaModel.click()
        browser.pause(medium)
        
        const clickonCarModel = $("//input[@id='make-BMW']/../div") 
        clickonCarModel.waitForExist()
        clickonCarModel.click()
        browser.pause(short)
     

        clickonMakeaModel.click()

        browser.pause(medium)
       



    


        
    })

    it("Select Price Range", () => {

        const clickonMonthlyprice = $("//div[contains(@data-component,'desktop-filters')]/descendant::h3[contains(@data-key,'Monthly')]/ancestor::button/div")
        clickonMonthlyprice.click()

        browser.pause(medium)
        const sliderleft = $ ("//div[@class='rc-slider-handle rc-slider-handle-1']")
        sliderleft.click()
        browser.pause(short)
        const sliderleftfocus = $("//div[contains(@class,'rc-slider-handle rc-slider-handle-1')]") 
        sliderleftfocus.dragAndDrop({ x: 40, y: 0 })
        browser.pause(long)
        clickonMonthlyprice.click()
        browser.pause(long)
    
       
    })


    it("Sorting low high", () => {

        const selectorder = $('(//div[@class="loader loader--fixed loader--relative"]/descendant::select)[2]')
        selectorder.waitForExist()
        selectorder.selectByVisibleText('Price (low-high)')
        browser.pause(long)
      
    
       
    })


    it("Sorting high low", () => {

        const selectorder = $('(//div[@class="loader loader--fixed loader--relative"]/descendant::select)[2]')
        selectorder.waitForExist()
        selectorder.selectByVisibleText('Price (high-low)')

        browser.pause(10000)
      
    
       
    })
 
});


